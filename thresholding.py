import numpy as np
import nibabel as nib
import imageio
from natsort import natsorted
import os
import cv2
import pandas as pd
import shutil, sys
from scipy import stats

# For each patient, the code goes through eyeball segmentations, calculates CSA of eyeball, puts those values in a list, then goes over the images again
# and select only one slice per patient. Going from top of scan to bottom once the 95% threshold is reached the slice is saved and
# there is a break in the code. This should get a slice which is a few slices up from the mid orbital level.

def selecting_slices(patient_number, input_path, output_path):
 os.chdir(input_path)
 for i in range(patient_number):
  print('Calculating eyeball CSA for patient number', i)
  areas_list=[]  #creating an empty list to hold values of segmented eyeball CSA
  for filename in natsorted (os.listdir(input_path)): #go over each image in test folder in order
    if filename.startswith('predict%d_'%i): #go over only through segmentation images but not the original slices
        img = cv2.imread(filename)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY) #convert to grayscale to be able to use count non zero function
        ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY) #threshold image
        area = cv2.countNonZero(thresh) #calculate segmentation area
        areas_list.append(area)

# go over images again and using the previously compiled list of areas choose one slice per patient that has an area equal or higher than the value for the 95% percentile
  for filename in natsorted (os.listdir(input_path), reverse=True):
    if filename.startswith('predict%d_'%i):
        img = cv2.imread(filename)
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY)
        area = cv2.countNonZero(thresh)
        newname=filename[7:]
        #if area == max(areas_list): #can use this instead of the following to lines to get the image with the largest eyeball CSA
        area_95=np.percentile(areas_list, 94)
        if area >= area_95:
          print(newname) # creating the name of the original file corresponding to the name of predicted file (without 'predict' part)
          shutil.copy(newname, output_path) #copying a slice corresponding to the selected segmentation from test forlder in eyeball model to test folder in temporalis model
          break

patient_numbers=[]
areas_list_temporalis=[]
def temporalis_csa(path_to_temporalis_segmentations, path_to_main_folder):
   os.chdir(path_to_temporalis_segmentations)
   # i=0
   for filename in natsorted (os.listdir(path_to_temporalis_segmentations)):
    if filename.startswith('predict'):
        # if filename.startswith('predict%d_'%i): #go over only through segmentation images but not the original slices
     img = cv2.imread(filename)
     gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY) #convert to grayscale to be able to use count non zero function
     ret, thresh = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY) #threshold image
     area_temporalis = cv2.countNonZero(thresh)
     patient_number=filename[7:]
     # print(filename)
     # print(area_temporalis) #calculate segmentation area
     areas_list_temporalis.append(area_temporalis)
     patient_numbers.append(patient_number)
     # i=i+1
   os.chdir(path_to_main_folder)
   temporalis={'Patient number ': patient_numbers, 'Temporalis CSA': areas_list_temporalis}
   CSA_TEMPORALIS=pd.DataFrame(temporalis)
   CSA_TEMPORALIS.to_csv('results.csv', mode='a')
