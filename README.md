# Temporalis Segmentation Pipeline


This code was written to automatically produce 2D temporalis muscle segmentations and calculate its cross-sectional area using 3D brain MRI scans (in NIfTI format) as input. 

To use this code you need to be familiar with creating a virtual environment and using pip. Some knowledge of using the command line is required. The code needs to be run using python 3.6. 

To set up and run the pipeline follow these steps: 

**1.** Create a virtual environment in **python 3.6** and install  **dependencies** either by using the **requirements.txt** file (compiled from a virtual environment in Ubuntu 18.04 LTS on a Windows computer) or pip installing the below listed dependencies separately(make sure to install the correct version if listed):

keras==2.2.4

tensorflow==1.14

segmentation_models

matplotlib

numpy

nibabel

natsort

imageio

scikit-image

simpleitk

pandas

cv2 (pip install opencv-python)

scipy

intensity-normalization package (current version v1.4.5) (pip install intensity-normalization==v1.4.5)


**IMPORTANT!** If manually installing all dependencies, unistall current version of h5py by running 'pip unistall h5py' and install an older version 'pip install h5py==2.10' (with the new version there are issues with loading the file containing pre-trained neural network weights (hdf5 file))

**2.** Install ants (Advanced Normalization tools in Python https://github.com/ANTsX/ANTsPy) according to your system requirements. 

**3.** Download pre-trained model weights from https://drive.google.com/drive/folders/1shgt5S6WFO3BJAKJfI22JSzKPVA3uFOQ?usp=sharing and add them to the repository folder (where all the downloaded scripts are). 

**4.** Create a folder called 'images' and add your data (3D Axial T1 contrast MRI scans in NIfTI format). Example data download guidelines are given at the bottom of the page. 
 

**5.** RUN the **main_full.py** script. You will be prompted to add the path to the main directory with all the scripts and the total number of images you put in the 'images' folder.  (**OPTIONAL**: if running on hpc run the -hpc option 'python3 main_full.py -hpc' instead)


Output temporalis CSA will be found in an excel file produced in the main directory **'results.csv'**. Original file names linked with renamed final file names will be found in **'data_dictionary.csv'** file. Selected slices for each image and the corresponding predicted segmentation images can be found in the 'temporalis_model/data/MR/test' folder.




**Downloading example data (TCGA-GBM dataset)**


1. Go to https://www.cancerimagingarchive.net/nbia-search/?CollectionCriteria=TCGA-GBM
2. Select and download axial t1 post-contrast images using patient identifiers and date of scan from the 'TCGA-GBM_AXIAL_T1_IMAGES' file attached. Press on the patient number, then on the correct date and select an axial t1 post contrast scan to add to cart. Download all selected images in the cart. 
3. Convert images from DICOM format into NIfTI (for example, by using MRIcroGL software that contains the **dcm2nii** application which can be downloaded at: https://www.nitrc.org/frs/?group_id=889)
4. After the images are converted they can be placed in the 'images' folder for running the pipeline. 

###################################################################################################
